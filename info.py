form = [
    # { # 此网站域名过期，良心网站，勿删
    #     'web_name': '错过电影网',
    #     'method': 'get',
    #     'params': 'wd',
    #     'url':'http://www.yixiuyu.com/index.php/vod/search.html?',
    #     'movie_title': '//span[@class="s_tit"]/a/strong/text()',
    #     'movie_href': '//span[@class="s_tit"]/a/@href',
    #     'domain': 'http://www.yixiuyu.com',
    # },
    {
        'web_name': '飘花电影网',
        'method': 'get',
        'params': 'searchword',
        'url': 'https://www.piaohuatv.org/search.php?',
        'movie_title': '//p[@class="name"]/text()',
        'movie_href': '//a[@class="li-hv"]/@href',
        'domain': 'https://www.piaohuatv.org',
    },
    {
        'web_name': '4k影院',
        'method': 'get',
        'params': 'wd',
        'url': 'https://www.4ktt.cn/index.php/vod/search.html?',
        'movie_title': '//div[@class="play-txt"]/h2/a/text()',
        'movie_href': '//div[@class="play-txt"]/h2/a/@href',
        'domain': 'https://www.4ktt.cn',
    },
    # { # 这个网站有点low，不支持url编码
    #     'web_name':'kk3电影',
    #     'params': 'searchword',
    #     'url': 'http://www.kk3.tv/search.asp?',
    #     'movie_title': '//div[@class="listBox"]/ul/li/a/text()',
    #     'movie_href': '//div[@class="listBox"]/ul/li/a/@href',
    #     'domain': 'http://www.kk3.tv',
    # }
]
forma = [
    {
        'web_name': 'A4yy',
        'url': 'http://aaqqy.com/vod-search-pg-1-wd-%s.html',
        'movie_title': '//h5/a/font/text()',
        'movie_href': '//ul[@id="contents"]/li/a/@href',
        'domain': 'http://aaqqy.com',
    },
]


DEFAULT_HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36',
    'Accept-Language': 'zh-CN,zh;q=0.9',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'Accept-Encoding': 'gzip, deflate',
}
