import json
import requests

def search(movie):
    '''
    web: 飞极速
    input: movie's name
    return: movie's href
    '''
    session = requests.session()

    session.headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.75 Safari/537.36',
        'Accept': '*/*',
        'Origin': 'http://www.fjisu.com'
    }
    # 模拟搜索，获取cookies
    session.get('http://www.fjisu.com/')
    session.get('http://www.fjisu.com/tv/6949/')

    url = 'http://v.mtyee.com/sssv.php?'
    params = {
        'top': 10,
        'q': movie, 
    }
    # 搜索电影
    response = session.get(url, params=params)
    response.encoding = 'utf-8'

    movie_info = response.text
    if movie_info == '﻿  []':
        print(f'\n>>>飞极速未找到电影<{movie}>')
        return

    movie_info = movie_info.lstrip().replace(' ', '').replace('﻿', '')


    info_dict = json.loads(movie_info)
    print('\n>>>飞极速电影搜索结果如下：')
    for i in info_dict:
        print()
        print(i.get('title'))
        print(i.get('url'))

if __name__ == '__main__':
    search('斗破苍穹')