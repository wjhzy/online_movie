import re
import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def search(movie):
    '''
    web: 看看屋
    input: movie's name
    return: movie's title and href
    '''
    url = f'https://www.kankanwu.com/index.php?s=vod-search-wd-{movie}-1-ajax'
    chrome_options = Options()
    # chrome_options.add_argument('--window-size=1366,768')
    chrome_options.add_argument('--headless')
    chrome = webdriver.Chrome(chrome_options=chrome_options)
    chrome.get(url)
    html = chrome.page_source
    title = re.findall(r'<h5><a href=".*?">(.*?)</a></h5>', html)
    href = re.findall(r'<h5><a href="(.*?)">.*?</a></h5>', html)
    if title == []:
        print(f'\n>>>看看屋未搜索到电影<{movie}>')
        return
    print('\n>>>看看屋电影搜索结果：')
    for t, h in zip(title, href):
        print()
        print(t.replace('<font color="red">', '').replace('</font>', ''))
        print(f'https://www.kankanwu.com{h}')
    time.sleep(0.5)
    chrome.quit()

if __name__ == '__main__':
    search('斗破苍穹')

    