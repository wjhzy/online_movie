#-*- coding:utf-8 -*-
import requests
from lxml import etree
from requests import RequestException

from info import DEFAULT_HEADERS, form, forma
from feijisu import search as fjisu
from kankanwu import search as kankan


class Movie(object):

    def __init__(self, movie):
        self.movie = movie
    
    def Search_Forma(self):
        for movie in forma:
            try:
                resp = requests.get(movie.get('url')%self.movie, headers=DEFAULT_HEADERS, timeout=8)
                if resp.status_code == 200:
                    resp = etree.HTML(resp.text)
                movie_title = resp.xpath(movie.get('movie_title'))
                movie_href = resp.xpath(movie.get('movie_href'))
                movie_hrefs = [movie.get('domain', '')+href for href in movie_href]
                if movie_title:
                    print('\n\n>>>'+movie.get('web_name')+'搜索到的结果如下：'+'\n')
                    for title, href in zip(movie_title, movie_hrefs):
                        print('电影名：', title)
                        print('链接：', href)
                        print()
                else:
                    print('\n>>>{web_name}-未搜索到资源<{movie_name}>'.format(
                        web_name=movie.get('web_name'), movie_name=self.movie)
                        )
            except RequestException as e:
                print(e)
        
    def Search_Form(self):
        for movie in form:
            try:
                if movie.get('method') == 'get':
                    params = {movie.get('params'): self.movie}
                    resp = requests.get(url=movie.get('url'), params=params, headers=DEFAULT_HEADERS, timeout=8)
                else:
                    data = {movie.get('data'): self.movie}
                    resp = requests.post(url=movie.get('url'), data=data, headers=DEFAULT_HEADERS, timeout=8)
                        
                
                if resp.status_code == 200:
                    resp = etree.HTML(resp.text)
                movie_title = resp.xpath(movie.get('movie_title'))
                movie_href = resp.xpath(movie.get('movie_href'))
                movie_hrefs = [movie.get('domain', '')+href for href in movie_href]
                if movie_title:
                    print('\n\n>>>'+movie.get('web_name')+'搜索到的结果如下：'+'\n')
                    for title, href in zip(movie_title, movie_hrefs):
                        print('电影名：', title)
                        print('链接：', href)
                        print()
                else:
                    print('\n>>>{web_name}-未搜索到资源<{movie_name}>'.format(
                        web_name=movie.get('web_name'), movie_name=self.movie)
                        )
            except RequestException as e:
                print(e)
            

if __name__ == '__main__':
    movie_name = '白蛇'
    fjisu(movie_name)
    kankan(movie_name)
    movie = Movie(movie_name)
    movie.Search_Form()
    movie.Search_Forma()
